//
//  ViewController.swift
//  AFNetworking-Test
//
//  Created by JoNATHAN GREENE on 6/26/15.
//  Copyright (c) 2015 Jonathan Green. All rights reserved.
//

import UIKit
import OAuthSwift

class ViewController:UIViewController,UITableViewDataSource,UITableViewDelegate {
   
    @IBOutlet weak var myTable: UITableView!
    
    var manager = GithubAPIClientManager.SharedInstance
    var repoArray:[AnyObject] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //// Showing that both are the same instance
        println(manager)
        println(manager)
    
        //// creates a delay before the auth menthod is called
        //// before the webviewcontroller was casuing Unbalanced calls to begin/end appearance transitions.
        
        var aDelay = NSTimer.scheduledTimerWithTimeInterval(2, target: self, selector:"auth", userInfo: nil, repeats: false)
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "fetchData:", name: "refreshMyTableView", object: nil)
    }
    
    func auth(){
        
        manager.authenticationRequest()

    }
    
    func fetchData(notification: NSNotification){
        
        repoArray = notification.object as! [AnyObject]
        myTable.reloadData()
    }
    
    //// This is a method of the UITableView Delegate that determines the number of sections in a table view
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        
        return 1
    }
    
    //// This is a method of the UITableView Delegate that determines the number of rows in each section
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       
        // #warning Incomplete method implementation.
        // Return the number of rows in the section.
        return repoArray.count
        
    }
    
    //// This is a method of the UITableView Delegate that determines the data assigned to the propteries of the tableview cell
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        var cell: TheCell = tableView.dequeueReusableCellWithIdentifier("myCell")
            as! TheCell
        cell.theLabel.text = repoArray[indexPath.row] as? String
        return cell
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}

