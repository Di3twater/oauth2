//
//  TheCell.swift
//  AFNetworking-Test
//
//  Created by JoNATHAN GREENE on 7/7/15.
//  Copyright (c) 2015 Jonathan Green. All rights reserved.
//

import UIKit

class TheCell: UITableViewCell {

    @IBOutlet weak var theLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        theLabel.text = "boom"
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
