//
//  NetworkManger.swift
//  AFNetworking-Test
//
//  Created by JoNATHAN GREENE on 7/1/15.
//  Copyright (c) 2015 Jonathan Green. All rights reserved.
//

import UIKit
import AFNetworking
import OAuthSwift
import SwiftyJSON

class GithubAPIClientManager: AFHTTPSessionManager {
    
  static let SharedInstance = GithubAPIClientManager()
    
    func authenticationRequest(){
        
        var repoData:[String] = []
        let gitAuthentication = OAuth2Swift(
            
            consumerKey: "b8473d86873ff201dc1f",
            consumerSecret: "6a1b33277d1222b391faf1ebb5c8a4d2dd6a3030",
            authorizeUrl: "https://github.com/login/oauth/authorize",
            accessTokenUrl: "https://github.com/login/oauth/access_token",
            responseType: "code"
        )
        
       //////////////////////// OAuth2.0////////////////////////
        
        /// by default iOS will push the authorization to a browser the authorize_url_handler allows you to assign an embeded web view to use for the authorization, this is done because while we could still iniate a callback url from the browser to the app through an appdelgate medthod, this current method only requires us to dismiss the view controller.
        gitAuthentication.authorize_url_handler = webViewController()
        
        ////An unguessable random string. It is used to protect against cross-site request forgery attacks.
        let state: String = generateStateWithLength(20) as String
        
        ////The authorizeWithCallbackURL method starts the authorization with the credentials passed into the OAuth2Swift class
        //// The issue remains that while authrization with the desired scope is successful the completion handlers do not fire
        gitAuthentication.authorizeWithCallbackURL( NSURL(string:"oauth-swift://oauth-callback/github")!, scope: "user,repo", state:state, success: {
            credential, response, parameters in

            var getRepo = self.GET("https://api.github.com/user/starred", parameters: ["access_token":credential.oauth_token], success: { (operation:NSURLSessionDataTask!, responseObject:AnyObject!) -> Void in
                
                var json = JSON(responseObject)
                
                for var i = 0; i < json.count; i++ {
                    
                    var starred = json[i]["name"].stringValue
                    repoData.append(starred)
                    
                }
                NSNotificationCenter.defaultCenter().postNotificationName("refreshMyTableView", object: repoData)
                
                }, failure: { (operaton:NSURLSessionDataTask!, error:NSError!) -> Void in
                
                    println(error)
                })
            
            }, failure: {(error:NSError!) -> Void in
                
                println(error)
        })

    }
    
}