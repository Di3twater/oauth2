//
//  webViewController.swift
//  AFNetworking-Test
//
//  Created by JoNATHAN GREENE on 7/1/15.
//  Copyright (c) 2015 Jonathan Green. All rights reserved.
//

import UIKit
import OAuthSwift

class webViewController: OAuthWebViewController, UIWebViewDelegate {
    
    var targetURL : NSURL = NSURL()
    let webView : UIWebView = UIWebView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        webView.frame = UIScreen.mainScreen().applicationFrame
        webView.scalesPageToFit = true
        webView.delegate = self
        view.addSubview(webView)
        loadAddressURL()

    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    override func handle(url: NSURL) {
        targetURL = url
        super.handle(url)
    }
    func loadAddressURL() {
        let req = NSURLRequest(URL: targetURL)
        self.webView.loadRequest(req)
    }
    
    // checks to make sure the callback url matches the url for the app
    func webView(webView: UIWebView, shouldStartLoadWithRequest request: NSURLRequest, navigationType: UIWebViewNavigationType) -> Bool {
        if let url = request.URL where (url.scheme == "oauth-swift"){
            self.dismissWebViewController()
        }
        return true
    }
    
}
